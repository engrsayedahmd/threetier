<?php

require_once 'app/general/functions.php';
require_once 'app/auth-controller.php';

authorizedUserRedirect('dashboard.php');

if(isset($_POST['login']))
{
    $values = array(
        'email' => $_POST['email'],
        'password' => $_POST['password']
    );

    userLogin($values);
}

?>

<?php setPageTitle('Login');  require_once 'header.php'; ?>

<div class="container">
    <div class="row justify-content-center mt-5">
        <div class="col-md-4">
            <div class="card">
                <div class="card-header">
                    Login
                </div>
                <div class="card-body">

                    <?php

                    if(isset($_GET['status']))
                    {
                        switch ($_GET['status'])
                        {
                            case 'fields_empty':
                                echo alert('Fields can not be empty!', 'warning');
                                break;
                            case 'exists_false':
                                echo alert("Account not found!", 'warning');
                                break;
                            case 'passw_wrong':
                                echo alert('Wrong password!', 'warning');
                                break;

                        }
                    }

                    ?>

                    <form action="login.php" method="post" class="needs-validation" novalidate enctype="multipart/form-data">
                        <div class="form-group">
                            <label for="email">Email address</label>
                            <input type="text" class="form-control" name="email" placeholder="Email" required>
                        </div>
                        <div class="form-group">
                            <label for="password">Password</label>
                            <input type="password" class="form-control" name="password" placeholder="Password" required>
                        </div>
                        <button type="submit" name="login" class="btn btn-primary">Login</button>
                        Not registered yet ? <a href="register.php">Register</a>
                    </form>
                </div>
            </div>

        </div>
    </div>
</div>


<?php require_once 'footer.php' ?>