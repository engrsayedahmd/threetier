<?php

require_once 'app/general/functions.php';
require_once 'app/auth-controller.php';

unauthorizedUserRedirect('login.php');

?>

<?php setPageTitle('Login');  require_once 'header.php'; ?>

    <div class="container">
        <div class="row">
            <div class="col-lg-12">
                <h1>
                    Dashboard <?php echo session_id() ?>
                </h1>
            </div>
        </div>
    </div>


<?php require_once 'footer.php' ?>