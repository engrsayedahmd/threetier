<?php
/**
 * Created by PhpStorm.
 * User: sayed
 * Date: 7/22/18
 * Time: 5:22 PM
 */

$page_title = null;

function setPageTitle($title)
{
    global $page_title;
    $page_title = $title;
}

function getPageTitle()
{
    global $page_title;
    echo $page_title;
}

function alert($msg, $msg_type)
{
    switch ($msg_type)
    {
        case 'warning':
            return '<div class="alert alert-warning">'.'* '.$msg.'</div>';
            break;
        case 'success':
            return '<div class="alert alert-success">'.'* '.$msg.'</div>';
            break;
        case 'info':
            return '<div class="alert alert-info">'.'* '.$msg.'</div>';
            break;
    }
}

