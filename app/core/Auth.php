<?php
/**
 * Created by PhpStorm.
 * User: sayed
 * Date: 9/5/18
 * Time: 1:12 PM
 */


namespace auth;

require_once 'Core.php';

use core\Core;

class Auth extends Core {

    public static function users_by_email($where) {

        return self::dbFetchWithCond('users', $where, 'single');

    }

    public static function user_reg($values) {

        return self::dbInsert('users', $values);

    }

}