<?php
/**
 * Created by PhpStorm.
 * User: sayed
 * Date: 7/21/18
 * Time: 2:30 PM
 */


require_once 'core/Auth.php';
require_once 'security/functions.php';
require_once 'db/db.php';

use auth\Auth as auth;

# SESSION WILL INITIATE IF NO SESSION EXISTS
if(session_status() == PHP_SESSION_NONE){
    //There is no active session
    session();
}

# DATABASE CONFIG
auth::dbConfig($db);

# USER LOGIN
function userLogin($values)
{
    $not_empty = !empty($values['email']) && !empty($values['password']);

    # CONTINUE IF FIELDS ARE FILLED
    if($not_empty == true)
    {
        $where = array(
            'email = ?' => $values['email']
        );

        $user = auth::users_by_email($where);

        # CONTINUE IF USER EXISTS
        if(count($user))
        {
            if(password_verify($values['password'], $user['password']))
            {
                $_SESSION['initiated'] = true;
                $_SESSION['authorized'] = $user['id'];
                $_SESSION['csrf_token'] = encryption(uniqid(true));

                if(isset($_SESSION['authorized']))
                {
                    header('Location: dashboard.php');
                }

            }
            else
            {
                # STATUS - PASSWORD WRONG
                header('Location: login.php?status=passw_wrong');
            }
        }
        else
        {
            # STATUS - USER DOES NOT EXISTS
            header('Location: login.php?status=exists_false');
        }

    }

    else
    {
        # STATUS - EMPTY FIELDS
        header('Location: login.php?status=fields_empty');
    }


}

function userReg($values)
{
    $name = safeString($values['name']);
    $email = safeEmail($values['email']);
    $safe_email = $email['safe_email'];
    $password = password_hash($values['password'], 1);

    $not_empty = !empty($values['name']) && !empty($values['email']) && !empty($values['password']);

    if($not_empty == true)
    {
        if($email['status'] === 'safe')
        {
            $user_info = array(
                null,
                $name,
                $safe_email,
                $password
            );

            $reg_status = auth::user_reg($user_info);

            if($reg_status == true)
            {
                header('Location: register.php?status=reg_success');
            }
        }
        else
        {
            # STATUS - INVALID EMAIL
            header('Location: register.php?status=email_invalid');
        }

    }
    else
    {
        # STATUS - EMPTY FIELDS
        header('Location: register.php?status=fields_empty');
    }
}
